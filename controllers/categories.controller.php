<?php

class ControllerCategories{

  /*=============================================
	AJOUTER CATEGORIE
	=============================================*/

  static public function ctrAjouterCategorie(){

		if(isset($_POST["addCategorie"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["addCategorie"])){

				$table = "categories";

				$datos = $_POST["addCategorie"];

				$reponse = ModelCategories::modelAjouterCategorie($table, $datos);

				if($reponse == "ok"){

          echo '<script>
								swal({
											 title: "Bravo!",
											 text: "Catégorie enregistré avec succès!",
											 icon: "success",
										 		}).then((value) => {
													 window.location = "categories";
												 });
								</script>';

				}


			}else{

        echo '<script>
          swal({
                 title: "ERROR!",
                 text: "Le nom de la catégorie ne peut pas être vide ou avoir des caractères spéciaux!",
                 icon: "warning",
                  }).then((value) => {
                     window.location = "categories";
                   });
          </script>';

			}

		}

	}

  /*=============================================
  AFFICHER CATEGORIES
  =============================================*/

  static public function ctrAfficherCategories($item, $valeur){

    $table = "categories";

    $reponse = ModelCategories::mdlAfficherCategories($table, $item, $valeur);

    return $reponse;

  }

  /*=============================================
	MODIFIER CATEGORIE
	=============================================*/

	static public function ctrModifierCategorie(){

		if(isset($_POST["modifierCategorie"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["modifierCategorie"])){

				$table = "categories";

				$datos = array("categorie"=>$_POST["modifierCategorie"],
							   "id"=>$_POST["idCategorie"]);

				$reponse = ModelCategories::mdlModifierCategorie($table, $datos);

				if($reponse == "ok"){

          echo '<script>
								swal({
											 title: "Bravo!",
											 text: "Catégorie modifié avec succès!",
											 icon: "success",
										 		}).then((value) => {
													 window.location = "categories";
												 });
								</script>';

				}


			}else{

        echo '<script>
          swal({
                 title: "ERROR!",
                 text: "Le nom de la catégorie ne peut pas être vide ou avoir des caractères spéciaux!",
                 icon: "warning",
                  }).then((value) => {
                     window.location = "categories";
                   });
          </script>';

			}

		}

	}

	/*=============================================
	SUPPRIMER CATEGORIE
	=============================================*/

	static public function ctrSupprimerCategorie(){

		if(isset($_GET["idCategorie"])){

			$table ="categories";
			$datos = $_GET["idCategorie"];

			$reponse = ModelCategories::mdlSupprimerCategorie($table, $datos);

			if($reponse == "ok"){

        echo '<script>
              swal({
                     title: "Bravo!",
                     text: "La catégorie à été supprimé!",
                     icon: "success",
                      }).then((value) => {
                         window.location = "categories";
                       });
              </script>';
			}
		}

	}

}
 ?>
