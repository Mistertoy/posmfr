<?php

class ControllerProduits{

  /*=============================================
	AFFICHER PRODUITS
	=============================================*/

	static public function ctrAfficherProduits($item, $valeur,$ordre){

		$table = "produits";

		$reponse = ModelProduits::mdlAfficherProduits($table, $item, $valeur,$ordre);

		return $reponse;

	}

  /*=============================================
	AJOUTER PRODUIT
	=============================================*/

	static public function ctrAjouterProduit(){

		if(isset($_POST["addDescription"])){

			if(preg_match('/^[a-zA-Z0-9áéíóúÁÉÍÓÚ ]+$/', $_POST["addDescription"]) &&
			   preg_match('/^[0-9]+$/', $_POST["addStock"]) &&
			   preg_match('/^[0-9.]+$/', $_POST["addPrixAchat"]) &&
			   preg_match('/^[0-9.]+$/', $_POST["addPrixVente"])){

		   	/*=============================================
				VALIDATION PHOTO
				=============================================*/

			   	$route = "views/img/produits/default/anonymous.png";

			   	if(isset($_FILES["addPhoto"]["tmp_name"])){

					list($largeur, $hauteur) = getimagesize($_FILES["addPhoto"]["tmp_name"]);

					$nouvLargeur = 500;
					$nouvHauteur = 500;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directoire = "views/img/produits/".$_POST["addCode"];

					mkdir($directoire, 0755);

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["addPhoto"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatoire = mt_rand(100,999);

						$route = "views/img/produits/".$_POST["addCode"]."/".$aleatoire.".jpg";

						$origine = imagecreatefromjpeg($_FILES["addPhoto"]["tmp_name"]);

						$destin = imagecreatetruecolor($nouvLargeur, $nouvHauteur);

						imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

						imagejpeg($destin, $route);

					}

					if($_FILES["addPhoto"]["type"] == "image/png"){

						/*=============================================
						ENREGISTRER L'IMAGE DANS LE RÉPERTOIRE
						=============================================*/

						$aleatoire = mt_rand(100,999);

						$route = "views/img/produits/".$_POST["addCode"]."/".$aleatoire.".png";

						$origine = imagecreatefrompng($_FILES["addPhoto"]["tmp_name"]);

						$destin = imagecreatetruecolor($nouvLargeur, $nouvHauteur);

						imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

						imagepng($destin, $route);

					}

				}

				$table = "produits";

				$datos = array("id_categorie" => $_POST["addCategorie"],
							   "code" => $_POST["addCode"],
							   "description" => $_POST["addDescription"],
							   "stock" => $_POST["addStock"],
							   "prix_achat" => $_POST["addPrixAchat"],
							   "prix_vente" => $_POST["addPrixVente"],
							   "photo" => $route);

				$reponse = ModelProduits::mdlAjouterProduit($table, $datos);

				if($reponse == "ok"){

					echo '<script>
								swal({
											 title: "Bravo!",
											 text: "Produit enregistré avec succès!",
											 icon: "success",
										 		}).then((value) => {
													 window.location = "produits";
												 });
								</script>';

				}else{
					echo '<script>
								swal({
											 title: "ERREUR!",
											 text: "Produit NON enregistré!",
											 icon: "warning",
										 		}).then((value) => {
													 window.location = "produits";
												 });
								</script>';

				}

			}else{

					echo '<script>
						swal({
									 title: "ERROR!",
									 text: "Le nom du produit ne peut pas être vide ou avoir des caractères spéciaux!",
									 icon: "warning",
										}).then((value) => {
											 window.location = "produits";
										 });
						</script>';
			}
		}

	}

	/*=============================================
	MODIFIER PRODUIT
	=============================================*/

	static public function ctrModifierProduit(){

		if(isset($_POST["modifDescription"])){

			if(preg_match('/^[a-zA-Z0-9áéíóúÁÉÍÓÚ ]+$/', $_POST["modifDescription"]) &&
			   preg_match('/^[0-9]+$/', $_POST["modifStock"]) &&
			   preg_match('/^[0-9.]+$/', $_POST["modifPrixAchat"]) &&
			   preg_match('/^[0-9.]+$/', $_POST["modifPrixVente"])){

		   		/*=============================================
				VALIDAR IMAGEN
				=============================================*/

			   	$route = $_POST["photoActuel"];

			   	if(isset($_FILES["addPhoto"]["tmp_name"]) && !empty($_FILES["addPhoto"]["tmp_name"])){

					list($largeur, $hauteur) = getimagesize($_FILES["addPhoto"]["tmp_name"]);

					$nouvLargeur = 500;
					$nouvHauteur = 500;

					/*=============================================
					CRÉATION DU RÉPERTOIRE POUR ENREGISTRER LA PHOTO
					=============================================*/

					$directorio = "views/img/produits/".$_POST["modifCode"];

					/*=============================================
					D'ABORD, NOUS DEMANDONS S'IL Y A UNE AUTRE IMAGE DANS LA BD
					=============================================*/

					if(!empty($_POST["photoActuel"]) && $_POST["photoActuel"] != "views/img/produits/default/anonymous.png"){

						unlink($_POST["photoActuel"]);

					}else{

						mkdir($directorio, 0755);

					}

					/*=============================================
					SELON LE TYPE D'IMAGE NOUS APPLIQUONS LES FONCTIONS PAR DÉFAUT DE PHP
					=============================================*/

					if($_FILES["addPhoto"]["type"] == "image/jpeg"){

						/*=============================================
						ENREGISTRER L'IMAGE DANS LE RÉPERTOIRE
						=============================================*/

						$aleatoire = mt_rand(100,999);

						$route = "views/img/produits/".$_POST["modifCode"]."/".$aleatoire.".jpg";

						$origine = imagecreatefromjpeg($_FILES["addPhoto"]["tmp_name"]);

						$destin = imagecreatetruecolor($nouvLargeur , $nouvHauteur);

						imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

						imagejpeg($destin, $route);

					}

					if($_FILES["addPhoto"]["type"] == "image/png"){

						/*=============================================
						ENREGISTRER L'IMAGE DANS LE RÉPERTOIRE
						=============================================*/

						$aleatoire = mt_rand(100,999);

						$route = "views/img/produits/".$_POST["modifCode"]."/".$aleatoire.".png";

						$origine = imagecreatefrompng($_FILES["addPhoto"]["tmp_name"]);

						$destin = imagecreatetruecolor($nouvLargeur, $nouvHauteur);

						imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

						imagepng($destin, $route);

					}

				}

				$table = "produits";

				$datos = array("id_categorie" => $_POST["modifCategorie"],
							   "code" => $_POST["modifCode"],
							   "description" => $_POST["modifDescription"],
							   "stock" => $_POST["modifStock"],
							   "prix_achat" => $_POST["modifPrixAchat"],
							   "prix_vente" => $_POST["modifPrixVente"],
							   "photo" => $route);

				$reponse = ModelProduits::mdlModifierProduit($table, $datos);

				if($reponse == "ok"){

					echo '<script>
								swal({
											 title: "Bravo!",
											 text: "Produit modifié avec succès!",
											 icon: "success",
										 		}).then((value) => {
													 window.location = "produits";
												 });
								</script>';

				}


			}else{

				echo '<script>
					swal({
								 title: "ERROR!",
								 text: "Le nom du produit ne peut pas être vide ou avoir des caractères spéciaux!",
								 icon: "warning",
									}).then((value) => {
										 window.location = "produits";
									 });
					</script>';
			}
		}

	}

	/*=============================================
	SUPPRIMER PRODUIT
	=============================================*/
	static public function ctrSupprimerProduit(){

		if(isset($_GET["idProduit"])){

			$table ="produits";
			$datos = $_GET["idProduit"];

			if($_GET["photo"] != "" && $_GET["photo"] != "views/img/produits/default/anonymous.png"){

				unlink($_GET["photo"]);
				rmdir('views/img/produits/'.$_GET["code"]);

			}

			$reponse = ModelProduits::mdlSupprimerProduit($table, $datos);

			if($reponse == "ok"){

				echo '<script>
					swal({
								 title: "Succès!",
								 text: "Produit supprimé avec succès!",
								 icon: "success",
							 }).then((result) => {
								 	if (result) {
										 window.location = "produits";
									 }
									 })
					</script>';

			}
		}


	}



}

 ?>
