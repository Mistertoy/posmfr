<?php

class ControllerClients{

	/*=============================================
	AJOUTER CLIENT
	=============================================*/

	static public function ctrAjouterClient(){

		if(isset($_POST["nomClient"])){

			if(preg_match('/^[a-zA-Z0-9áéíóúÁÉÍÓÚ ]+$/', $_POST["nomClient"]) &&
			   preg_match('/^[0-9]+$/', $_POST["docClient"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["emailClient"]) &&
			   preg_match('/^[()\-0-9 ]+$/', $_POST["phoneClient"]) &&
			   preg_match('/^[#\.\-a-zA-Z0-9 ]+$/', $_POST["adresseClient"])){

			   	$table = "clients";

			   	$datos = array("nom"=>$_POST["nomClient"],
					           "cni"=>$_POST["docClient"],
					           "mail"=>$_POST["emailClient"],
					           "phone"=>$_POST["phoneClient"],
					           "adresse"=>$_POST["adresseClient"],
					           "date_naissance"=>$_POST["dateNaissance"]);

			   	$reponse = ModelClients::mdlAjouterClient($table, $datos);

			   	if($reponse == "ok"){

            echo '<script>
  								swal({
  											 title: "Bravo!",
  											 text: "Client enregistré avec succès!",
  											 icon: "success",
  										 		}).then((value) => {
  													 window.location = "clients";
  												 });
  								</script>';

				}

			}else{

        echo '<script>
          swal({
                 title: "ERROR!",
                 text: "Le nom du client ne peut pas être vide ou avoir des caractères spéciaux!",
                 icon: "warning",
                  }).then((value) => {
                     window.location = "clients";
                   });
          </script>';

			}

		}

	}

	/*=============================================
	AFFICHER CLIENTS
	=============================================*/

	static public function ctrAfficherClients($item, $valeur){

		$table = "clients";

		$reponse = ModelClients::mdlAfficherClients($table, $item, $valeur);

		return $reponse;

	}

	/*=============================================
	MODIFIER CLIENTS
	=============================================*/

	static public function ctrModifierClient(){

		if(isset($_POST["modifClient"])){

			if(preg_match('/^[a-zA-Z0-9áéíóúÁÉÍÓÚ ]+$/', $_POST["modifClient"]) &&
			   preg_match('/^[0-9]+$/', $_POST["modifCni"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["modifMail"]) &&
			   preg_match('/^[()\-0-9 ]+$/', $_POST["modifPhone"]) &&
			   preg_match('/^[#\.\-a-zA-Z0-9 ]+$/', $_POST["modifAdresse"])){

			   	$table = "clients";

			   	$datos = array("id"=>$_POST["idClient"],
			   				   "nom"=>$_POST["modifClient"],
					           "cni"=>$_POST["modifCni"],
					           "mail"=>$_POST["modifMail"],
					           "phone"=>$_POST["modifPhone"],
					           "adresse"=>$_POST["modifAdresse"],
					           "date_naissance"=>$_POST["modifDateNais"]);

			   	$reponse = ModelClients::mdlModifierClient($table, $datos);

			   	if($reponse == "ok"){

            echo '<script>
  								swal({
  											 title: "Bravo!",
  											 text: "Client modifié avec succès!",
  											 icon: "success",
											 		}).then((value) => {
												 		window.location = "clients";
  										 		});
  								</script>';
				}

			}else{

        echo '<script>
					swal({
								 title: "ERROR!",
								 text: "Le nom du client ne peut pas être vide ou avoir des caractères spéciaux!",
								 icon: "warning",
									}).then((value) => {
										 window.location = "clients";
									 });
					</script>';



			}

		}

	}

	/*=============================================
	SUPPRIMER CLIENT
	=============================================*/

	static public function ctrSupprimerClient(){

		if(isset($_GET["idClient"])){

			$table ="clients";
			$datos = $_GET["idClient"];

			$reponse = ModelClients::mdlSupprimerClient($table, $datos);

			if($reponse == "ok"){

        echo '<script>
					swal({
								 title: "Succès!",
								 text: "Client supprimé avec succès!",
								 icon: "success",
							 }).then((result) => {
								 	if (result) {
										 window.location = "clients";
									 }
									 })
					</script>';

			}

		}

	}

}

 ?>
