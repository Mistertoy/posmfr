<?php

class ControllerUsers{

	/*=============================================
	CONNEXION UTILISATEUR
	=============================================*/

	public function ctrConnexionUser(){

		if(isset($_POST["connexUser"])){

			if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["connexUser"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["connexMotpass"])){

				$crypter = crypt($_POST["connexMotpass"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
				$table = "users";

				$user = "user";
				$valeur = $_POST["connexUser"];

				$reponse= ModelUsers::mdlAfficherUsers($table, $user, $valeur);

				// var_dump($reponse);

				if($reponse["user"] == $_POST["connexUser"] && $reponse["password"] == $crypter){

					if($reponse["etat"] == 1){

						$_SESSION["sesionActive"] = "ok";
						$_SESSION["id"] = $reponse["id"];
						$_SESSION["nom"] = $reponse["nom"];
						$_SESSION["user"] = $reponse["user"];
						$_SESSION["photo"] = $reponse["photo"];
						$_SESSION["profil"] = $reponse["profil"];

						/*=============================================
						ENREGISTRER LA DERNIÈRE DATE DE CONNEXION
						=============================================*/

						date_default_timezone_set('Europe/Paris');

						$dateA = date('Y-m-d');
						$heure = date('H:i:s');

						$dateActuelle = $dateA.' '.$heure;

						$item1 = "dernier_login";
						$valeur1 = $dateActuelle;

						$item2 = "id";
						$valeur2 = $reponse["id"];

						$dernierlogin = ModelUsers::mdlActualisationUser($table, $item1, $valeur1, $item2, $valeur2);

						if($dernierlogin == "ok"){

							echo '<script>

								window.location = "accueil";

							</script>';

						}

					}else{

						echo '<br>
							<div class="alert alert-danger">Utilisateur pas encore activé</div>';

					}


				}else{

					// echo '<br><div class="alert alert-danger">La connexion a échoué, veuillez réessayer</div>';
					echo '<script type="text/javascript">',
					     'swal({
										  title: "ERROR !",
										  text: "Nom dutilisateur ou mot de passe incorrect",
										  icon: "warning",
										}).then((value) => {
												  window.location = "utilisateurs";
												});',
					     '</script>';

				}

			}

		}

	}

	/*=============================================
	AJOUTER UTILISATEUR
	=============================================*/
	static public function ctrAjouterUtilisateur(){

		if(isset($_POST['nomUser'])){

			if(preg_match('/^[a-zA-Z0-9áéíóúÁÉÍÓÚ ]+$/', $_POST["nomComplet"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["nomUser"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["motPasse"])){

				/*=============================================
				VALIDATION PHOTO
				=============================================*/

				$route = "";

				if(isset($_FILES["nouvellePhoto"]["tmp_name"])){

					list($largeur, $hauteur) = getimagesize($_FILES["nouvellePhoto"]["tmp_name"]);

					$nouvLargeur = 500;
					$nouvHauteur = 500;

				/*=============================================
				CREATION DU RÉPERTOIRE POUR ENREGISTRER LA PHOTO DE L'UTILISATEUR
				=============================================*/
				$repertoire = "views/img/utilisateurs/".$_POST["nomUser"];

					mkdir($repertoire, 0755);

			/*=============================================
				SELON LE TYPE D'IMAGE NOUS APPLIQUONS LES FONCTIONS PAR DÉFAUT DE PHP
				=============================================*/

				if($_FILES["nouvellePhoto"]["type"] == "image/jpeg"){
					/*=============================================
						ENREGISTRER L'IMAGE DANS LE RÉPERTOIRE
						=============================================*/
						$aleatoire = mt_rand(100,999);

						$route = "views/img/utilisateurs/".$_POST["nomUser"]."/".$aleatoire.".jpg";

						$origine = imagecreatefromjpeg($_FILES["nouvellePhoto"]["tmp_name"]);

						$destin = imagecreatetruecolor($nouvLargeur, $nouvHauteur);

						imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

						imagejpeg($destin, $route);

					}

					if($_FILES["nouvellePhoto"]["type"] == "image/png"){
						/*=============================================
							ENREGISTRER L'IMAGE DANS LE RÉPERTOIRE
							=============================================*/
							$aleatoire = mt_rand(100,999);

							$route = "views/img/utilisateurs/".$_POST["nomUser"]."/".$aleatoire.".png";

							$origine = imagecreatefrompng($_FILES["nouvellePhoto"]["tmp_name"]);

							$destin = imagecreatetruecolor($nouvLargeur, $nouvHauteur);

							imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

							imagepng($destin, $route);

						}
				}

				/*=============================================
				INFOS UTILISATEUR
				=============================================*/

				$table = "users";

				$crypter = crypt($_POST["motPasse"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$datos = array("nom" => $_POST["nomComplet"],
									 "user" => $_POST["nomUser"],
									 "motpasse" => $crypter,
									 "profil" => $_POST["profil"],
									 "photo"=>$route);

				$reponse = ModelUsers::mdlEnregistrerUser($table, $datos);

				if($reponse == "ok"){

					echo '<script>
								swal({
											 title: "Bravo!",
											 text: "Utilisateur enregistré avec succès!",
											 icon: "success",
										 		}).then((value) => {
													 window.location = "utilisateurs";
												 });
								</script>';
				}else{
					echo '<script>
								swal({
											 title: "Erreur",
											 text: "Utilisateur NON enregistré",
											 icon: "success",
										 		});
								</script>';
				}

			}else{

						echo '<script>
							swal({
										 title: "ERROR!",
										 text: "Le nom d utilisateur ne peut pas être vide ou avoir des caractères spéciaux!",
										 icon: "warning",
											}).then((value) => {
												 window.location = "utilisateurs";
											 });
							</script>';
					}
		}

	}

	/*=============================================
	AFFICHER TOUS LES UTILISATEURS
	=============================================*/

	static public function ctrAfficherUsers($item, $valeur){

		$table = "users";

		$reponse = ModelUsers::mdlAfficherUsers($table, $item, $valeur);

		return $reponse;
	}

	/*=============================================
	MODIFIER UTILISATEUR
	=============================================*/

	static public function ctrModifierUser(){

		if(isset($_POST["modifierUser"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["modifierNom"])){

				/*=============================================
				VALIDATION PHOTO
				=============================================*/

				$route = $_POST["photoActuel"];

				if(isset($_FILES["modifierPhoto"]["tmp_name"]) && !empty($_FILES["modifierPhoto"]["tmp_name"])){

					list($largeur, $hauteur) = getimagesize($_FILES["modifierPhoto"]["tmp_name"]);

					$nouvLargeur = 500;
					$nouvHauteur = 500;

					/*=============================================
					CRÉATION DU RÉPERTOIRE POUR ENREGISTRER LA PHOTO DE L'UTILISATEUR
					=============================================*/

					$repertoire = "views/img/utilisateurs/".$_POST["modifierUser"];

					/*=============================================
					D'ABORD, NOUS DEMANDONS S'IL Y A UNE AUTRE IMAGE DANS LA BD
					=============================================*/

					if(!empty($_POST["photoActuel"])){

						unlink($_POST["photoActuel"]);

					}else{

						mkdir($repertoire, 0755);

					}

					/*=============================================
					SELON LE TYPE D'IMAGE NOUS APPLIQUONS LES FONCTIONS PAR DÉFAUT DE PHP
					=============================================*/

					if($_FILES["modifierPhoto"]["type"] == "image/jpeg"){

						/*=============================================
						ENREGISTRER L'IMAGE DANS LE RÉPERTOIRE JPG
						=============================================*/

						$aleatoire = mt_rand(100,999);

						$route = "views/img/utilisateurs/".$_POST["modifierUser"]."/".$aleatoire.".jpg";

						$origine = imagecreatefromjpeg($_FILES["modifierPhoto"]["tmp_name"]);

						$destin = imagecreatetruecolor($nouvLargeur, $nouvHauteur);

						imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

						imagejpeg($destin, $route);

					}

					if($_FILES["modifierPhoto"]["type"] == "image/png"){

						/*=============================================
						ENREGISTRER L'IMAGE DANS LE RÉPERTOIRE PNG
						=============================================*/

						$aleatoire = mt_rand(100,999);

						$route = "views/img/utilisateurs/".$_POST["modifierUser"]."/".$aleatoire.".png";

						$origine = imagecreatefrompng($_FILES["modifierPhoto"]["tmp_name"]);

						$destin = imagecreatetruecolor($nouvLargeur, $nouvHauteur);

						imagecopyresized($destin, $origine, 0, 0, 0, 0, $nouvLargeur, $nouvHauteur, $largeur, $hauteur);

						imagepng($destin, $route);

					}

				}

				$table = "users";

				if($_POST["modifierPassword"] != ""){

					if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["modifierPassword"])){

						$crypter = crypt($_POST["modifierPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					}else{

						echo '<script>
							swal({
										 title: "ERROR!",
										 text: "Le mot de passe ne peut pas être vide ou contenir des caractères spéciaux!",
										 icon: "warning",
											}).then((value) => {
												 window.location = "utilisateurs";
											 });
							</script>';

					}

				}else{

					$crypter = $_POST["passwordActuel"];

				}

				$datos = array("nom" => $_POST["modifierNom"],
							   "user" => $_POST["modifierUser"],
							   "password" => $crypter,
							   "profil" => $_POST["modifierProfil"],
							   "photo" => $route);

				$reponse = ModelUsers::mdlModifierUser($table, $datos);

				if($reponse == "ok"){

					echo '<script>
								swal({
											 title: "Bravo!",
											 text: "Utilisateur modifié avec succès!",
											 icon: "success",
										 		}).then((value) => {
													 window.location = "utilisateurs";
												 });
								</script>';

				}


			}else{

				echo '<script>
					swal({
								 title: "ERROR!",
								 text: "Le nom d utilisateur ne peut pas être vide ou avoir des caractères spéciaux!",
								 icon: "warning",
									}).then((value) => {
										 window.location = "utilisateurs";
									 });
					</script>';

			}

		}

	}

	/*=============================================
	SUPPRIMER UTILISATEUR
	=============================================*/

	static public function ctrSupprimerUser(){

		if(isset($_GET["idUser"])){

			$table ="users";
			$datos = $_GET["idUser"];

			if($_GET["photoUser"] != ""){

				unlink($_GET["photoUser"]);
				rmdir('views/img/utilisateurs/'.$_GET["user"]);

			}

			$reponse = ModelUsers::mdlSupprimerUser($table, $datos);

			if($reponse == "ok"){

				echo '<script>
					swal({
								 title: "Succès!",
								 text: "Utilisateur supprimé avec succès!",
								 icon: "success",
							 }).then((result) => {
								 	if (result) {
										 window.location = "utilisateurs";
									 }
									 })
					</script>';

			}else{
				echo '<script>
							alert("Rien passé!");
							</script>';
			}

		}

	}

}
