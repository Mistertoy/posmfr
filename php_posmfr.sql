-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 20 mai 2020 à 13:36
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `php_posmfr`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `categorie`, `date`) VALUES
(1, 'Ordinateurs', '2020-05-03 11:31:26'),
(5, 'Dure Disque', '2020-05-19 09:25:20'),
(8, 'Cables', '2020-05-19 09:33:45');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `cni` int(11) NOT NULL,
  `email` text NOT NULL,
  `telephone` text NOT NULL,
  `adresse` text NOT NULL,
  `datenaissance` date NOT NULL,
  `achats` int(11) NOT NULL,
  `dernierachat` datetime NOT NULL,
  `date` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `nom`, `cni`, `email`, `telephone`, `adresse`, `datenaissance`, `achats`, `dernierachat`, `date`) VALUES
(11, 'Pato', 369874125, 'pato@pato.fr', '(125) 897-8966', '18 rue de lakanal', '1975-12-12', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Coco', 4430861, 'mpo@kio.com', '(365) 897-4566', '8 Rue de la chane', '1984-09-05', 0, '0000-00-00 00:00:00', '2020-05-18 12:17:00'),
(10, 'Alvaro Rocha', 3698745, 'avito20@gmail.com', '(071) 234-5678', '8 rue de la chaine', '1984-09-20', 0, '0000-00-00 00:00:00', '2020-05-18 08:28:14');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produit` text NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `code` text NOT NULL,
  `description` text NOT NULL,
  `photo` text NOT NULL,
  `stock` int(11) NOT NULL,
  `prix_achat` float NOT NULL,
  `prix_vente` float NOT NULL,
  `ventes` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `produit`, `id_categorie`, `code`, `description`, `photo`, `stock`, `prix_achat`, `prix_vente`, `ventes`, `date`) VALUES
(9, '', 0, 'ccfg', 'cable HDMI', 'views/img/produits/ccfg/152.jpg', 10, 80, 112, 0, '2020-05-19 09:39:49');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
