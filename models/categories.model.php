<?php

require_once "connexion.php";

class ModelCategories{
  /*=============================================
	AJOUTER CATEGORIE
	=============================================*/
  static public function modelAjouterCategorie($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("INSERT INTO $table(categorie) VALUES (:categorie)");

		$stmt->bindParam(":categorie", $datos, PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

  /*=============================================
	AFFICHER CATEGORIES
	=============================================*/

	static public function mdlAfficherCategories($table, $item, $valeur){

		if($item != null){

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valeur, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

  /*=============================================
	MODIFIER CATEGORIE
	=============================================*/

	static public function mdlModifierCategorie($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("UPDATE $table SET categorie = :categorie WHERE id = :id");

		$stmt -> bindParam(":categorie", $datos["categorie"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datos["id"], PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	SUPPRIMER CATEGORIE
	=============================================*/

	static public function mdlSupprimerCategorie($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("DELETE FROM $table WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}


}

?>
