<?php

require_once "connexion.php";

class ModelClients{

	/*=============================================
	AJOUTER CLIENT
	=============================================*/

	static public function mdlAjouterClient($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("INSERT INTO $table(nom, cni, email, telephone, adresse, datenaissance) VALUES (:nom, :cni, :email, :telephone, :adresse, :datenaissance)");

		$stmt->bindParam(":nom", $datos["nom"], PDO::PARAM_STR);
		$stmt->bindParam(":cni", $datos["cni"], PDO::PARAM_INT);
		$stmt->bindParam(":email", $datos["mail"], PDO::PARAM_STR);
		$stmt->bindParam(":telephone", $datos["phone"], PDO::PARAM_STR);
		$stmt->bindParam(":adresse", $datos["adresse"], PDO::PARAM_STR);
		$stmt->bindParam(":datenaissance", $datos["date_naissance"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	AFFICHER CLIENT
	=============================================*/

	static public function mdlAfficherClients($table, $item, $valeur){

		if($item != null){

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valeur, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MODIFIER CLIENT
	=============================================*/

	static public function mdlModifierClient($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("UPDATE $table SET nom = :nom, cni = :cni, email = :email, telephone = :telephone, adresse = :adresse, datenaissance = :datenaissance WHERE id = :id");

		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
		$stmt->bindParam(":nom", $datos["nom"], PDO::PARAM_STR);
		$stmt->bindParam(":cni", $datos["cni"], PDO::PARAM_INT);
		$stmt->bindParam(":email", $datos["mail"], PDO::PARAM_STR);
		$stmt->bindParam(":telephone", $datos["phone"], PDO::PARAM_STR);
		$stmt->bindParam(":adresse", $datos["adresse"], PDO::PARAM_STR);
		$stmt->bindParam(":datenaissance", $datos["date_naissance"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	SUPPRIMER CLIENT
	=============================================*/

	static public function mdlSupprimerClient($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("DELETE FROM $table WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR CLIENTE
	=============================================*/

	static public function mdlMettreJourClient($table, $item1, $valeur1, $valeur){

		$stmt = Connexion::seConnecter()->prepare("UPDATE $table SET $item1 = :$item1 WHERE id = :id");

		$stmt -> bindParam(":".$item1, $valeur1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $valeur, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

}
?>
