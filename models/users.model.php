<?php

require_once "connexion.php";

class ModelUsers{

	/*=============================================
	AFFICHER UTILISATEURS
	=============================================*/

	public static function mdlAfficherUsers($table, $item, $valeur){

		if($item != null){

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valeur, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table");

			$stmt -> execute();

			return $stmt -> fetchAll();
		}


		$stmt -> close();

		$stmt = null;
}

	/*=============================================
	ENREGISTRER UTILISATEUR
	=============================================*/

	public static function mdlEnregistrerUser($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("INSERT INTO $table(nom, user, password, profil, photo) VALUES (:newnom, :newuser, :motpasse, :newprofil, :newphoto)");

			$stmt->bindParam(":newnom", $datos["nom"],
			PDO::PARAM_STR);
			$stmt->bindParam(":newuser", $datos["user"], PDO::PARAM_STR);
			$stmt->bindParam(":motpasse", $datos["motpasse"], PDO::PARAM_STR);
			$stmt->bindParam(":newprofil", $datos["profil"], PDO::PARAM_STR);
			$stmt->bindParam(":newphoto", $datos["photo"], PDO::PARAM_STR);

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

		$stmt->close();

		$stmt = null;
	}

	/*=============================================
	MODIFIER UTILISATEUR
	=============================================*/

static public function mdlModifierUser($table, $datos){

	$stmt = Connexion::seConnecter()->prepare("UPDATE $table SET nom = :nom, password = :password, profil = :profil, photo = :photo WHERE user = :user");

	$stmt -> bindParam(":nom", $datos["nom"], PDO::PARAM_STR);
	$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
	$stmt -> bindParam(":profil", $datos["profil"], PDO::PARAM_STR);
	$stmt -> bindParam(":photo", $datos["photo"], PDO::PARAM_STR);
	$stmt -> bindParam(":user", $datos["user"], PDO::PARAM_STR);

	if($stmt -> execute()){

		return "ok";

	}else{

		return "error";

	}

	$stmt -> close();

	$stmt = null;

}

	/*=============================================
	SUPPRIMER UTILISATEUR
	=============================================*/

	static public function mdlSupprimerUser($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("DELETE FROM $table WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;


	}


	/*=============================================
	METTRE À JOUR L'UTILISATEUR
	=============================================*/

	static public function mdlActualisationUser($table, $item1, $valeur1, $item2, $valeur2){

		$stmt = Connexion::seConnecter()->prepare("UPDATE $table SET $item1 = :$item1 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valeur1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valeur2, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

}
