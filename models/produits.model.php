<?php

require_once "connexion.php";

class ModelProduits{

  /*=============================================
	AFFICHER PRODUITS
	=============================================*/

	static public function mdlAfficherProduits($table, $item, $valeur, $ordre){

		if($item != null){

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table WHERE $item = :$item ORDER BY id DESC");

			$stmt -> bindParam(":".$item, $valeur, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Connexion::seConnecter()->prepare("SELECT * FROM $table ORDER BY $ordre DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

  /*=============================================
	AJOUTER PRODUIT
	=============================================*/
  static public function mdlAjouterProduit($table, $datos){

    $stmt = Connexion::seConnecter()->prepare("INSERT INTO $table(id_categorie, code, description, photo, stock, prix_achat, prix_vente) VALUES (:id_categorie, :code, :description, :photo, :stock, :prix_achat, :prix_vente)");

		$stmt->bindParam(":id_categorie", $datos["id_categorie"], PDO::PARAM_INT);
		$stmt->bindParam(":code", $datos["code"], PDO::PARAM_STR);
		$stmt->bindParam(":description", $datos["description"], PDO::PARAM_STR);
		$stmt->bindParam(":photo", $datos["photo"], PDO::PARAM_STR);
		$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
		$stmt->bindParam(":prix_achat", $datos["prix_achat"], PDO::PARAM_STR);
		$stmt->bindParam(":prix_vente", $datos["prix_vente"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

  /*=============================================
	MODIFIER PRODUIT
	=============================================*/
	static public function mdlModifierProduit($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("UPDATE $table SET id_categorie = :id_categorie, description = :description, photo = :photo, stock = :stock, prix_achat = :prix_achat, prix_vente = :prix_vente WHERE code = :code");

		$stmt->bindParam(":id_categorie", $datos["id_categoria"], PDO::PARAM_INT);
		$stmt->bindParam(":code", $datos["code"], PDO::PARAM_STR);
		$stmt->bindParam(":description", $datos["description"], PDO::PARAM_STR);
		$stmt->bindParam(":photo", $datos["photo"], PDO::PARAM_STR);
		$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
		$stmt->bindParam(":prix_achat", $datos["prix_achat"], PDO::PARAM_STR);
		$stmt->bindParam(":prix_vente", $datos["prix_vente"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt->close();
		$stmt = null;

	}

  /*=============================================
	SUPPRIMER PRODUIT
	=============================================*/

	static public function mdlSupprimerProduit($table, $datos){

		$stmt = Connexion::seConnecter()->prepare("DELETE FROM $table WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MISE À JOUR DU PRODUIT
	=============================================*/

	static public function mdlMiseaJourProduit($table, $item1, $valeur1, $valeur){

		$stmt = Connexion::seConnecter()->prepare("UPDATE $table SET $item1 = :$item1 WHERE id = :id");

		$stmt -> bindParam(":".$item1, $valeur1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $valeur, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	AFFICHER LES VENTES
	=============================================*/

	static public function mdlAfficherSommeVetes($table){

		$stmt = Connexion::seConnecter()->prepare("SELECT SUM(ventes) as total FROM $table");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
	}


}
?>
