<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php include "breadcrumb.php"; ?>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <button class="btn btn-primary" data-toggle="modal" data-target="#modalAjouterCategorie">Ajouter catégorie</button>

    </div>
      <div class="card-body">

        <table class="table table-bordered table-hover tables">
          <thead>
            <tr>
              <th>#</th>
              <th>Catégorie</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $item = null;
            $valeur = null;

            $categories = ControllerCategories::ctrAfficherCategories($item, $valeur);

            foreach ($categories as $key => $value) {

            echo '
            <tr>
              <td>'.($key+1).'</td>
              <td class="text-uppercase">'.$value["categorie"].'</td>

              <td>
                <div class="btn-group">
                  <button class="btn btn-warning btnModifierCategorie" idCategorie="'.$value["id"].'" data-toggle="modal" data-target="#modalModifierCategorie"><i class="fas fa-pencil-alt"></i></button>

                  <button class="btn btn-danger btnSupprimerCategorie"  idCategorie="'.$value["id"].'"><i class="fas fa-trash-alt"></i></button>
                </div>
              </td>
            </tr>';
          }
          ?>
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          Footer
        </div>
      </div>

</section>

</div>
  <!-- MODAL AJOUTER CATEGORIE -->

  <div class="modal fade" id="modalAjouterCategorie">
    <div class="modal-dialog">
      <div class="modal-content">

        <form role="form" method="post" enctype="multipart/form-data">

        <!-- Modal Header -->
        <div class="modal-header" style="background-color: #007bff; color:#fff;">
          <h4 class="modal-title">Ajouter catégorie</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card-body">

            <!-- NOM  -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-th"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="addCategorie" id="nomUser" placeholder="Nom catégorie" required>
            </div>



          </div>
        </div> <!-- Fin Modal body -->

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

            <button type="submit" class="btn btn-primary">Enregistrer</button>
          </div>

          <!-- CONTROLLER AJOUTER UTILISATEUR -->
          <?php
            $ajouterCategorie = new ControllerCategories();
            $ajouterCategorie -> ctrAjouterCategorie();
          ?>

        </form>
      </div>
    </div>
  </div>



  <!--=====================================
  MODAL MODIFIER CATEGORIE
  ======================================-->

  <div class="modal fade" id="modalModifierCategorie">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">

          <!-- Modal Header -->

          <div class="modal-header" style="background-color: #007bff; color:#fff;">
            <h4 class="modal-title">Modifier catégorie</h4>
            <button type="button" class="close" data-dismiss="modal">×</button>
          </div>

          <!-- Modal body -->

          <div class="modal-body">
            <div class="card-body">

              <!-- MODIFIER NOM CATEGORIE -->

              <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-th"></i></span>
                </div>
                  <input type="text" class="form-control input-lg" id="modifierCategorie" name="modifierCategorie" required>

                  <input type="hidden" name="idCategorie" id="idCategorie" required>
              </div>

            </div>
          </div>

          <!--=====================================
          FOOTER MODAL
          ======================================-->

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
            <button type="submit" class="btn btn-primary">Modifier</button>
          </div>

          <!-- CONTROLLER MODIFIER UTILISATEUR -->
       <?php
            $modifierCategorie = new ControllerCategories();
            $modifierCategorie -> ctrModifierCategorie();
          ?>

        </form>
      </div>
    </div>
  </div>

<!-- /.content-wrapper -->
<?php

  $supprimerCategorie = new ControllerCategories();
  $supprimerCategorie -> ctrSupprimerCategorie();

?>
