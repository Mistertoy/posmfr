<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php include "breadcrumb.php"; ?>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <button class="btn btn-primary" data-toggle="modal" data-target="#modalAjouterUtilisateur">Ajouter utilisateur</button>

    </div>
        <div class="card-body">

          <table class="table table-bordered table-hover tables">
            <thead>
            <tr>
              <th>#</th>
              <th>Nom</th>
              <th>Utilisateur</th>
              <th>Photo</th>
              <th>Profil</th>
              <th>Etat</th>
              <th>Dernier connexion</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
              <?php
              $item = null;
              $valeur = null;
              $utilisateurs = ControllerUsers::ctrAfficherUsers ($item, $valeur);

           foreach ($utilisateurs as $key => $value){
            echo '<tr>
              <td>'.($key+1).'</td>
              <td>'.$value["nom"].'</td>
              <td>'.$value["user"].'</td>';
              if($value["photo"] != ""){

                echo '<td><img src="'.$value["photo"].'" class="img-thumbnail" width="40px"></td>';

              }else{

                echo '<td><img src="views/img/utilisateurs/default/anonymous.png" class="img-thumbnail" width="40px"></td>';

              }

              echo '<td>'.$value["profil"].'</td>';

              if($value["etat"] != 0){

                echo '<td><button class="btn btn-success btn-xs btnActiver" idUser="'.$value["id"].'" etatUser="0">Activé</button></td>';

              }else{

                echo '<td><button class="btn btn-danger btn-xs btnActiver" idUser="'.$value["id"].'" etatUser="1">Désactivé</button></td>';

              }

              echo '<td>'.$value["dernier_login"].'</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning btnModifierUser" idUser="'.$value["id"].'" data-toggle="modal" data-target="#modalModifierUtilisateur"><i class="fas fa-pencil-alt"></i></button>

                  <button class="btn btn-danger btnSupprimerUser"  idUser="'.$value["id"].'" photoUser="'.$value["photo"].'" user="'.$value["user"].'"><i class="fas fa-trash-alt"></i></button>
                </div>
              </td>
            </tr>';
          }
          ?>
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          Footer
        </div>
      </div>

</section>

</div>
  <!-- MODAL AJOUTER UTILISATEUR-->

  <div class="modal fade" id="modalAjouterUtilisateur">
    <div class="modal-dialog">
      <div class="modal-content">

        <form role="form" method="post" enctype="multipart/form-data">

        <!-- Modal Header -->
        <div class="modal-header" style="background-color: #007bff; color:#fff;">
          <h4 class="modal-title">Ajouter Utilisateur</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card-body">

            <!-- NOM  -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="nomUser" id="nomUser" placeholder="Utilisateur" required>
            </div>

            <!-- NOM COMPLET -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="nomComplet" placeholder="Nom complet" required>
            </div>

            <!-- MOT DE PASSE -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-key"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="motPasse" placeholder="Mot de passe" required>
            </div>

            <!-- CONFIRMATION MOT DE PASSE -->
            <!-- <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-key"></i></span>
              </div>
                <input type="password" class="form-control input-lg" name="motPasse2" placeholder="Mot de passe" required>
            </div> -->

            <!-- PROFIL -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-users"></i></span>
              </div>
              <select class="form-control" name="profil">
                <option value="">Sélectionner le profil</option>
                <option value="administrateur">Administrateur</option>
                <option value="special">Comptable</option>
                <option value="vendeur">Vendeur</option>
              </select>
            </div>

            <!-- PHOTO -->

             <div class="form-group">
                <div class="panel">Télécharger une photo</div>
                <input type="file" class="nouvellePhoto" name="nouvellePhoto">
                <p class="help-block">200 Mo maximum</p>
                <img src="views/img/utilisateurs/default/anonymous.png" class="img-thumbnail apercu" width="100px">
             </div>

          </div>
        </div> <!-- Fin Modal body -->

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

            <button type="submit" class="btn btn-primary">Enregistrer</button>
          </div>

          <!-- CONTROLLER AJOUTER UTILISATEUR -->
          <?php
            $ajouterUtilisateur = new ControllerUsers();
            $ajouterUtilisateur -> ctrAjouterUtilisateur();
          ?>

        </form>
      </div>
    </div>
  </div>



  <!--=====================================
  MODAL MODIFIER UTILISATEUR
  ======================================-->

  <div class="modal fade" id="modalModifierUtilisateur">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">

          <!-- Modal Header -->

          <div class="modal-header" style="background-color: #007bff; color:#fff;">
            <h4 class="modal-title">Modifier Utilisateur</h4>
            <button type="button" class="close" data-dismiss="modal">×</button>
          </div>

          <!-- Modal body -->

          <div class="modal-body">
            <div class="card-body">

              <!-- MODIFIER NOM (NOMCOMPLET) -->

              <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
                </div>
                  <input type="text" class="form-control input-lg" id="modifierNom" name="modifierNom" required>
              </div>


              <!-- MODIFIER UTILISATEUR -->

              <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                </div>
                  <input type="text" class="form-control input-lg" id="modifierUser" name="modifierUser" value="" readonly>
                </div>


              <!-- MODIFIER MOT DE PASSE -->

              <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-key"></i></span>
                </div>
                  <input type="password" class="form-control input-lg" name="modifierPassword" placeholder="Entrez le nouveau mot de passe">
                  <input type="hidden" id="passwordActuel" name="passwordActuel">
              </div>

              <!-- MODIFIER PROFIL -->

              <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-users"></i></span>
                </div>
                  <select class="form-control input-lg" name="modifierProfil">

                    <option value="" id="modifierProfil"></option>
                    <option value="Administrateur">Administrateur</option>
                    <option value="Especial">Especial</option>
                    <option value="Vendeur">Vendeur</option>

                  </select>
                </div>


              <!-- MODIFIER PHOTO -->

              <div class="form-group">
                <div class="panel">Télécharger une photo</div>
                <input type="file" class="nouvellePhoto" name="modifierPhoto">
                <p class="help-block">200 Mo maximum</p>
                <img src="views/img/utilisateurs/default/anonymous.png" class="img-thumbnail apercu" width="100px">
                <input type="hidden" name="photoActuel" id="photoActuel">
              </div>
            </div>
          </div>

          <!--=====================================
          FOOTER MODAL
          ======================================-->

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
            <button type="submit" class="btn btn-primary">Modifier</button>
          </div>

          <!-- CONTROLLER MODIFIER UTILISATEUR -->
       <?php
            $modifierUtilisateur = new ControllerUsers();
            $modifierUtilisateur -> ctrModifierUser();
          ?>

        </form>
      </div>
    </div>
  </div>

<!-- /.content-wrapper -->
<?php

$supprimerUtilisateur = new ControllerUsers();
$supprimerUtilisateur -> ctrSupprimerUser();

?>
