<div class="login-box">
  <div class="login-logo">
    <a href="index.php"><b>BTS </b>- SIO 2020</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Connexion</p>

      <form method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Utilisateur" name="connexUser">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="mot de passe" name="connexMotpass">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Se connecter</button>
          </div>

        </div>
        <?php
        $login = new ControllerUsers();
        $login -> ctrConnexionUser();

         ?>

      </form>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
