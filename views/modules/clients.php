<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Clients</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="accueil">Accueil</a></li>
              <li class="breadcrumb-item active">Clients</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <button class="btn btn-primary" data-toggle="modal" data-target="#modalAjouterClient">Ajouter client</button>

    </div>
        <div class="card-body">

          <table class="table table-bordered table-striped tables">
            <thead>
            <tr>
              <th>#</th>
              <th>Nom</th>
              <th>CNI</th>
              <th>Email</th>
              <th>Téléphone</th>
              <th>Adresse</th>
              <th>Date de naissance</th>
              <th>Total Achat</th>
              <th>Dernier achat</th>
              <th>Connexion</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
              <?php
              $item = null;
              $valeur = null;
              $clients = ControllerClients::ctrAfficherClients ($item, $valeur);

           foreach ($clients as $key => $value){
            echo '<tr>
              <td>'.($key+1).'</td>
              <td>'.$value["nom"].'</td>
              <td>'.$value["cni"].'</td>
              <td>'.$value["email"].'</td>
              <td>'.$value["telephone"].'</td>
              <td>'.$value["adresse"].'</td>
              <td>'.$value["datenaissance"].'</td>
              <td>'.$value["achats"].'</td>
              <td>'.$value["dernierachat"].'</td>
              <td>'.$value["date"].'</td>

              <td>
                <div class="btn-group">
                  <button class="btn btn-warning btnModifierClient" data-toggle="modal" data-target="#modalModifierClient" idClient="'.$value["id"].'"><i class="fas fa-pencil-alt"></i></button>';

                if($_SESSION["profil"] == "administrateur"){

                  echo '<button class="btn btn-danger btnSupprimerClient" idClient="'.$value["id"].'"><i class="fas fa-trash-alt"></i></button>';
                }
                echo '</div>
              </td>
            </tr>';
          }
          ?>
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          Footer
        </div>
      </div>

</section>

</div>
  <!-- MODAL AJOUTER CLIENT-->

  <div class="modal fade" id="modalAjouterClient">
    <div class="modal-dialog">
      <div class="modal-content">

        <form role="form" method="post">

        <!-- Modal Header -->
        <div class="modal-header" style="background-color: #007bff; color:#fff;">
          <h4 class="modal-title">Ajouter Client</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card-body">

            <!-- NOM  -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="nomClient"  placeholder="Nom client" required>
            </div>

            <!-- CNI -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="far fa-id-card"></i></span>
              </div>
              <input type="number" class="form-control input-lg" name="docClient" placeholder="CNI" required>
            </div>

            <!-- EMAIL -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="far fa-envelope"></i></span>
              </div>
              <input type="email" class="form-control input-lg" name="emailClient" placeholder="e-mail" required>
            </div>

            <!-- TELEPHONE -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-phone"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="phoneClient" data-inputmask="'mask':'(999) 999-9999'" placeholder="Téléphone" required>
            </div>

            <!-- ADRESSE -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="adresseClient" placeholder="Adresse" required>
            </div>

            <!-- DATE NAISSANCE -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-birthday-cake"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="dateNaissance" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-inputmask-placeholder="aaaa/mm/jj" required>
            </div>

          </div>
        </div> <!-- Fin Modal body -->

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

            <button type="submit" class="btn btn-primary">Enregistrer</button>
          </div>

          <!-- CONTROLLER AJOUTER UTILISATEUR -->
          <?php
            $ajouterClient = new ControllerClients();
            $ajouterClient -> ctrAjouterClient();
          ?>

        </form>
      </div>
    </div>
  </div>



  <!--=====================================
  MODAL MODIFIER CLIENT
  ======================================-->

  <div class="modal fade" id="modalModifierClient">
    <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" method="post">

          <!-- Modal Header -->

          <div class="modal-header" style="background-color: #007bff; color:#fff;">
            <h4 class="modal-title">Modifier Client</h4>
            <button type="button" class="close" data-dismiss="modal">×</button>
          </div>

          <!-- Modal body -->

          <div class="modal-body">
            <div class="card-body">

              <!-- MODIFIER NOM -->

              <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fas fa-user-tie"></i></span>
                </div>
                  <input type="text" class="form-control input-lg" name="modifClient" id="modifClient" required >
                  <input type="hidden" id="idClient" name="idClient">
              </div>


              <!-- CIN -->

              <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="far fa-id-card"></i></span>
                </div>
                  <input type="number" class="form-control input-lg" name="modifCni" id="modifCni" required >
                </div>

                <!-- EMAIL -->
                <div class="input-group mb-2">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="far fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control input-lg" name="modifMail" id="modifMail" required >
                </div>

                <!-- TELEPHONE -->
                <div class="input-group mb-2">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                  </div>
                  <input type="text" class="form-control input-lg" name="modifPhone" id="modifPhone" data-inputmask="'mask':'(999) 999-9999'" >
                </div>

                <!-- ADRESSE -->
                <div class="input-group mb-2">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                  </div>
                  <input type="text" class="form-control input-lg" name="modifAdresse" id="modifAdresse" >
                </div>

                <!-- DATE NAISSANCE -->
                <div class="input-group mb-2">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-birthday-cake"></i></span>
                  </div>
                  <input type="text" class="form-control input-lg" name="modifDateNais" id="modifDateNais" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-inputmask-placeholder="aaaa/mm/jj" >
                </div>

            </div>
          </div>

          <!--=====================================
          FOOTER MODAL
          ======================================-->

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
            <button type="submit" class="btn btn-primary">Modifier Client</button>
          </div>

          <!-- CONTROLLER MODIFIER UTILISATEUR -->
           <?php
              $modifierClient = new ControllerClients();
              $modifierClient -> ctrModifierClient();
            ?>

        </form>


      </div>
    </div>
  </div>


<?php

$supprimerClient = new ControllerClients();
$supprimerClient -> ctrSupprimerClient();

?>
