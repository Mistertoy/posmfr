<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php include "breadcrumb.php"; ?>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <button class="btn btn-primary" data-toggle="modal" data-target="#modalAjouterProduit">Ajouter Produit</button>

    </div>
        <div class="card-body">

          <table class="table table-bordered table-hover tableProduits">
            <thead>
            <tr>
              <th>#</th>
              <th>Image</th>
              <th>Code</th>
              <th>Description</th>
              <th>Catégorie</th>
              <th>Stock</th>
              <th>Prix d'achat</th>
              <th>Prix de vente</th>
              <th>Date création</th>
              <th>Actions</th>
            </tr>
            </thead>

          </table>

          <input type="hidden" value="<?php echo $_SESSION['profil']; ?>" id="profilCache">

        </div>
        <div class="card-footer">
          Footer
        </div>
      </div>

</section>

</div>
  <!-- MODAL AJOUTER PRODUIT-->

  <div class="modal fade" id="modalAjouterProduit">
    <div class="modal-dialog">
      <div class="modal-content">

        <form role="form" method="post" enctype="multipart/form-data">

        <!-- Modal Header -->
        <div class="modal-header" style="background-color: #007bff; color:#fff;">
          <h4 class="modal-title">Ajouter Produit</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card-body">

            <!-- SELECTION CATEGORIE -->

            <div class="input-group mb-2">
              <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-th"></i></span>

                <select class="form-control input-lg" id="addCategorie" name="addCategorie" required>
                  <option value="">Sélectionnez une catégorie</option>

                  <?php

                  $item = null;
                  $valeur = null;

                  $categories = ControllerCategories::ctrAfficherCategories($item, $valeur);

                  foreach ($categories as $key => $value) {

                    echo '<option value="'.$value["id"].'">'.$value["categorie"].'</option>';
                  }

                  ?>
                </select>
              </div>
            </div>

            <!-- CODE  -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-tag"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="addCode" id="addCode" placeholder="Code produit" required>
            </div>

            <!-- DESCRIPTION -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-shapes"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="addDescription" placeholder="Description Produit" required>
            </div>

            <!-- STOCK -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="far fa-arrow-alt-circle-right"></i></span>
              </div>
              <input type="number" class="form-control input-lg" name="addStock" min="0" placeholder="Quantité" required>
            </div>


            <!-- PRIX ACHAT -->
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-arrow-up"></i></span>
                </div>
                <input type="number" class="form-control" name="addPrixAchat" id="addPrixAchat" min="0" step="any" placeholder="Prix d'achat" required>
              </div>
              </div>

              <!-- PRIX VENTE -->
              <div class="col-xs-12 col-sm-6">
                <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-arrow-down"></i></span>
                </div>
                <input type="number" class="form-control" name="addPrixVente" id="addPrixVente" min="0" step="any" placeholder="Prix vente" required>
              </div>
              </div>

              <!-- CHECKBOX POURCENTAGE-->

                 <div class="col-xs-6">
                   <div class="form-group">
                     <label>
                       <input type="checkbox" class="minimal pourcentage" checked>
                       Utiliser le pourcentage
                     </label>
                   </div>
                 </div>

              <!-- ENTRER POURCENTAGE -->

                 <div class="col-xs-6">
                     <div class="input-group mb-2">
                     <div class="input-group-append">
                      <span class="input-group-text"><i class="fa fa-percent"></i></span>
                      </div>
                     <input type="number" class="form-control input-lg addPourcentage" min="0" value="40" required>
                   </div>
                   </div>
                 </div>

            </div>

            <!-- PHOTO PRODUIT -->

             <div class="form-group">
                <div class="panel">Télécharger une photo</div>
                <input type="file" class="addPhoto" name="addPhoto">
                <p class="help-block">200 Mo maximum</p>
                <img src="views/img/produits/default/anonymous.png" class="img-thumbnail apercu" width="100px">
             </div>

        </div> <!-- Fin Modal body -->

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

            <button type="submit" class="btn btn-primary">Enregistrer Produit</button>
          </div>

          <!-- CONTROLLER AJOUTER PRODUIT -->
          <?php
            $ajouterProduit = new ControllerProduits();
            $ajouterProduit -> ctrAjouterProduit();
          ?>

        </form>
      </div>
    </div>
  </div>



  <!--=====================================
  MODAL MODIFIER PRODUIT
  ======================================-->

  <div class="modal fade" id="modalModifierProduit">
    <div class="modal-dialog">
      <div class="modal-content">

        <form role="form" method="post" enctype="multipart/form-data">

        <!-- Modal Header -->
        <div class="modal-header" style="background-color: #007bff; color:#fff;">
          <h4 class="modal-title">Modifier Produit</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="card-body">

            <!-- SELECTION CATEGORIE -->

            <div class="input-group mb-2">
              <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-th"></i></span>

                <select class="form-control input-lg" name="modifCategorie" >
                  <option id="modifCategorie"></option>

                  <?php

                  $item = null;
                  $valeur = null;

                  $categories = ControllerCategories::ctrAfficherCategories($item, $valeur);

                  foreach ($categories as $key => $value) {

                    echo '<option value="'.$value["id"].'">'.$value["categorie"].'</option>';
                  }

                  ?>
                </select>
              </div>
            </div>
            <!-- <div class="input-group mb-2">
              <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-th"></i></span>

                <select class="form-control input-lg"  name="modifCategorie" readonly required>
                  <option id="modifCategorie"></option>

                </select>
              </div>
            </div> -->

            <!-- CODE  -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-tag"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="modifCode" id="modifCode" readonly required>
            </div>

            <!-- DESCRIPTION -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-shapes"></i></span>
              </div>
              <input type="text" class="form-control input-lg" name="modifDescription" id="modifDescription" required>
            </div>

            <!-- STOCK -->
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="far fa-arrow-alt-circle-right"></i></span>
              </div>
              <input type="number" class="form-control input-lg" name="modifStock" id="modifStock" min="0" required>
            </div>


            <!-- PRIX ACHAT -->
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-arrow-up"></i></span>
                </div>
                <input type="number" class="form-control" name="modifPrixAchat" id="modifPrixAchat" min="0" step="any" required>
              </div>
              </div>

              <!-- PRIX VENTE -->
              <div class="col-xs-12 col-sm-6">
                <div class="input-group mb-2">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-arrow-down"></i></span>
                </div>
                <input type="number" class="form-control" name="modifPrixVente" id="modifPrixVente" min="0" step="any" required>
              </div>
              </div>

              <!-- CHECKBOX POURCENTAGE-->

                 <div class="col-xs-6">
                   <div class="form-group">
                     <label>
                       <input type="checkbox" class="minimal pourcentage" checked>
                       Utiliser le pourcentage
                     </label>
                   </div>
                 </div>

              <!-- ENTRE POURCENTAGE -->

                 <div class="col-xs-6">
                     <div class="input-group mb-2">
                     <div class="input-group-append">
                      <span class="input-group-text"><i class="fa fa-percent"></i></span>
                      </div>
                     <input type="number" class="form-control input-lg addPourcentage" min="0" value="40" required>
                   </div>
                   </div>
                 </div>

            </div>

            <!-- PHOTO PRODUIT -->

             <div class="form-group">
                <div class="panel">Télécharger une photo</div>
                <input type="file" class="nouvellePhoto" name="addPhoto">
                <p class="help-block">200 Mo maximum</p>
                <img src="views/img/produits/default/anonymous.png" class="img-thumbnail apercu" width="100px">

                <input type="hidden" name="photoActuel" id="photoActuel">
             </div>

        </div> <!-- Fin Modal body -->

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>

            <button type="submit" class="btn btn-primary">Enregistrer</button>
          </div>

          <!-- CONTROLLER AJOUTER PRODUIT -->
          <?php
            $modifierProduit = new ControllerProduits();
            $modifierProduit -> ctrModifierProduit();
          ?>

        </form>
      </div>
    </div>
  </div>

<!-- /.content-wrapper -->
<?php

  $supprimerUtilisateur = new ControllerProduits();
  $supprimerUtilisateur -> ctrSupprimerProduit();

?>
