<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>

    </ul>


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="dropdown user user-menu">
        <a href="#"class="dropdown-toggle" data-toggle="dropdown">

          <?php

					if($_SESSION["photo"] != ""){

            echo '<img src="'.$_SESSION["photo"].'" class="img-circle elevation-2" alt="User Image" width="32px">
            <span class="hidden-xs">Administrator</span>';
          }else{
            echo '<img src="views/img/utilisateurs/default/anonymous.png" class="user-image">';
          }
          ?>
        </a>
        <ul class="dropdown-menu">
          <li class="user-body">
            <div class="pull-right">
              <a href="se-deconnecter" class="btn btn-default btn-flat">Se déconnecter</a>
            </div>
          </li>
        </ul>
      </li>

    </ul>

  </nav>
  <!-- /.navbar -->
