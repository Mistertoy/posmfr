<?php

session_start();

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BTS | Informatique</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="views/img/template/favicon.png">
  <!-- ****************************************
  STYLES CSS
  *********************************************-->
  <!-- Bootstrap 4 -->

  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="views/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="views/plugins/fontawesome-free/css/all.min.css">

  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="views/dist/css/AdminLTE.css">

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="views/dist/css/adminlte.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- DataTables CSS-->
  <link rel="stylesheet" href="views/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="views/plugins/datatables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="views/plugins/datatables/Responsive-2.2.3/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="views/plugins/datatables/Responsive-2.2.3/css/responsive.bootstrap4.min.css">

  <!-- fullCalendar -->
  <link rel="stylesheet" href="views/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="views/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="views/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="views/plugins/fullcalendar-bootstrap/main.min.css">
  <link rel="stylesheet" href="views/plugins/fullcalendar-list/main.min.css">


<!-- ****************************************
PLUGINS JAVASCRIPT
*********************************************-->

  <!-- jQuery -->
  <script src="views/plugins/jquery/jquery.min.js"></script>

  <!-- Bootstrap 4 -->
  <script src="views/plugins/bootstrap/js/bootstrap.min.js"></script>

  <!-- AdminLTE App -->
  <script src="views/dist/js/adminlte.min.js"></script>

  <!-- DataTables JS-->
  <script src="views/plugins/datatables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="views/plugins/datatables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script src="views/plugins/datatables/Responsive-2.2.3/js/dataTables.responsive.min.js"></script>
  <script src="views/plugins/datatables/Responsive-2.2.3/js/responsive.bootstrap4.min.js"></script>

  <!-- SweetAlert 2 -->
  <script src="views/plugins/sweetalert/sweetalert.min.js"></script>

  <!-- By default SweetAlert2 doesn't support IE. To enable IE 11 support, include Promise polyfill:-->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script> -->

  <!-- InputMask-->
  <script src="views/plugins/inputmask/inputmask.js"></script>

  <script src="views/plugins/inputmask/inputmask/inputmask.date.extensions.js"></script>
  <script src="views/plugins/inputmask/inputmask/inputmask.extensions.js"></script>
  <script src="views/plugins/inputmask/inputmask/jquery.inputmask.js"></script>

  <!-- fullCalendar 2.2.5 -->
<script src="views/plugins/moment/moment.min.js"></script>
<script src="views/plugins/fullcalendar/main.min.js"></script>
<script src="views/plugins/fullcalendar-daygrid/main.min.js"></script>
<script src="views/plugins/fullcalendar-timegrid/main.min.js"></script>
<script src="views/plugins/fullcalendar-interaction/main.min.js"></script>
<script src="views/plugins/fullcalendar-bootstrap/main.min.js"></script>
<script src="views/plugins/fullcalendar/locales/fr.js"></script>
<script src="views/plugins/fullcalendar-list/main.min.js"></script>


</head>
<body class="hold-transition sidebar-mini layout-fixed sidebar-collapse login-page">
<!-- Site wrapper -->


  <?php
    if (isset($_SESSION["sesionActive"]) && $_SESSION["sesionActive"] == "ok"){

      echo '<div class="wrapper" style="width:100%;">';
      // navbar
      include "modules/header.php";

      // Main Sidebar
      include "modules/menu.php";


      // Contenu
        if(isset($_GET["lien"])){
          if($_GET["lien"] == "accueil" ||
             $_GET["lien"] == "utilisateurs" ||
             $_GET["lien"] == "categories" ||
             $_GET["lien"] == "produits" ||
             $_GET["lien"] == "clients" ||
             $_GET["lien"] == "ventes" ||
             $_GET["lien"] == "ajouter-vente" ||
             $_GET["lien"] == "ajouter-vente" ||
             $_GET["lien"] == "se-deconnecter"){
               include "modules/".$_GET["lien"].".php";
             }else{

               include "modules/404.php";

             }

        }else{
          include "modules/accueil.php";
        }

        // Footer
        include "modules/footer.php";

        echo '</div>'; //fin  wrapper

      }else{
        //page login
        include "modules/login.php";
  }

  ?>




<script src="views/js/template.js"></script>
<script src="views/js/users.js"></script>
<script src="views/js/categories.js"></script>
<script src="views/js/produits.js"></script>
<script src="views/js/clients.js"></script>

</body>
</html>
