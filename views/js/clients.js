/*=============================================
MODIFIER CLIENT
=============================================*/
$(".tables").on("click", ".btnModifierClient", function(){


	let idClient = $(this).attr("idClient");
	console.log("clike client: ",idClient);
	let datos = new FormData();
    datos.append("idClient", idClient);

    $.ajax({

      url:"ajax/clients.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(reponse){

	      $("#idClient").val(reponse["id"]);
	      $("#modifClient").val(reponse["nom"]);
	      $("#modifCni").val(reponse["cni"]);
	      $("#modifMail").val(reponse["email"]);
	      $("#modifPhone").val(reponse["telephone"]);
	      $("#modifAdresse").val(reponse["adresse"]);
	      $("#modifDateNais").val(reponse["datenaissance"]);
	  	}
  		})
})

/*=============================================
SUPPRIMER CLIENT
=============================================*/
$(".tables").on("click", ".btnSupprimerClient", function(){

	let idClient = $(this).attr("idClient");

  swal({
	  title: "Attention",
	  text: "Voulez-vous vraiment supprimer le client?",
	  icon: "warning",
	  buttons: ["Annuler", "Confirmer"],

	}).then((result) => {
	  if (result) {
	    window.location = "index.php?lien=clients&idClient="+idClient;
	  }
	})

})
