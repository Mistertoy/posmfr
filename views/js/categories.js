/*=============================================
MODIFIER CATEGORIE
=============================================*/
$(".tables").on("click", ".btnModifierCategorie", function(){

	var idCategorie = $(this).attr("idCategorie");

	var datos = new FormData();
	datos.append("idCategorie", idCategorie);

	$.ajax({
		url: "ajax/categories.ajax.php",
		method: "POST",
      	data: datos,
      	cache: false,
     	contentType: false,
     	processData: false,
     	dataType:"json",
     	success: function(reponse){
        console.log(reponse["categorie"]);
     		$("#modifierCategorie").val(reponse["categorie"]);
     		$("#idCategorie").val(reponse["id"]);

     	}

	})


})

/*=============================================
SUPPRIMER CATEGORIE
=============================================*/
$(".tables").on("click", ".btnSupprimerCategorie", function(){

	 var idCategorie = $(this).attr("idCategorie");

   swal({
 	  title: "Attention",
 	  text: "Voulez-vous vraiment supprimer la catégorie?",
 	  icon: "warning",
 	  buttons: ["Annuler", "Confirmer"],

 	}).then((result) => {
 	  if (result) {
 	    window.location = "index.php?lien=categories&idCategorie="+idCategorie;
 	  }
 	})

})
