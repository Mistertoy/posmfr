/*=============================================
CARGAR LA TABLA DINÁMICA DE PRODUCTOS
=============================================*/

$.ajax({

	url: "ajax/datatable-produits.ajax.php",
	success:function(reponse){

		// console.log("reponse", reponse);

	}

})

$('.tableProduits').DataTable( {
  "ajax": "ajax/datatable-produits.ajax.php?profilCache="+profilCache,
	"paging": true,
	"lengthChange": false,
	"searching": true,
	"ordering": true,
	"info": true,
	"autoWidth": false,
	"responsive": true,
  "deferRender": true,
	"retrieve": true,
	"processing": true,
  "language": {
                "sEmptyTable":     "Aucune donnée disponible dans le tableau",
                "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Afficher _MENU_ éléments",
                "sLoadingRecords": "Chargement...",
                "sProcessing":     "Traitement...",
                "sSearch":         "Rechercher :",
                "sZeroRecords":    "Aucun élément correspondant trouvé",
                "oPaginate": {
                  "sFirst":    "Premier",
                  "sLast":     "Dernier",
                  "sNext":     "Suivant",
                  "sPrevious": "Précédent"
                },
                "oAria": {
                  "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                  "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                },
                "select": {
                        "rows": {
                          "_": "%d lignes sélectionnées",
                          "0": "Aucune ligne sélectionnée",
                          "1": "1 ligne sélectionnée"
                        }
                }
              }

} );

/*=============================================
CAPTURANDO LA CATEGORIA PARA ASIGNAR CÓDIGO
=============================================*/
// $("#addCategorie").change(function(){
//
// 	var idCategorie = $(this).val();
//
// 	var datos = new FormData();
//   	datos.append("idCategorie", idCategorie);
//
//   	$.ajax({
//
//       url:"ajax/produits.ajax.php",
//       method: "POST",
//       data: datos,
//       cache: false,
//       contentType: false,
//       processData: false,
//       dataType:"json",
//       success:function(reponse){
//
//       	if(!reponse){
//
//       		var nouveauCode = idCategorie+"01";
//       		$("#addCode").val(nouveauCode);
//
//       	}else{
//
//       		var nouveauCode = Number(reponse["code"]) + 1;
//           	$("#addCode").val(nouveauCode);
//
//       	}
//
//       }
//
//   	})
//
// })

/*=============================================
AGREGANDO PRECIO DE VENTA
=============================================*/
$("#addPrixAchat, #modifPrixAchat").change(function(){

	if($(".pourcentage").prop("checked")){

		var valeurPourcentage = $(".addPourcentage").val();

		var pourcentage = Number(($("#addPrixAchat").val()*valeurPourcentage/100))+Number($("#addPrixAchat").val());

		var modifierPourcentage = Number(($("#modifPrixAchat").val()*valeurPourcentage/100))+Number($("#modifPrixAchat").val());

		$("#addPrixVente").val(pourcentage);
		$("#addPrixVente").prop("readonly",true);

		$("#modifPrixVente").val(modifierPourcentage);
		$("#modifPrixVente").prop("readonly",true);

	}

})

/*=============================================
CAMBIO DE pourcentage
=============================================*/
$(".addPourcentage").change(function(){

	if($(".pourcentage").prop("checked")){

		var valeurPourcentage = $(this).val();

		var pourcentage = Number(($("#addPrixAchat").val()*valeurPourcentage/100))+Number($("#addPrixAchat").val());

		var modifierPourcentage = Number(($("#modifPrixAchat").val()*valeurPourcentage/100))+Number($("#modifPrixAchat").val());

		$("#addPrixVente").val(pourcentage);
		$("#addPrixVente").prop("readonly",true);

		$("#modifPrixVente").val(modifierPourcentage);
		$("#modifPrixVente").prop("readonly",true);

	}

})

$(".pourcentage").on("ifUnchecked",function(){

	$("#addPrixVente").prop("readonly",false);
	$("#modifPrixVente").prop("readonly",false);

})

$(".pourcentage").on("ifChecked",function(){

	$("#addPrixVente").prop("readonly",true);
	$("#modifPrixVente").prop("readonly",true);

})

/*=============================================
TELECHARGER PHOTO PRODUIT
=============================================*/

$(".addPhoto").change(function(){

	var image = this.files[0];

	/*=============================================
  	VALIDAMOS EL FORMATO DE LA image SEA JPG O PNG
  	=============================================*/

  	if(image["type"] != "image/jpeg" && image["type"] != "image/png"){

  		$(".addPhoto").val("");

        swal({
           title: "Erreur de type d'image",
           text: "¡L'image doit être au format JPG ou PNG!",
           icon: "warning",
           button: "Fermer!",

         });

  	}else if(image["size"] > 2000000){

  		$(".addPhoto").val("");

        swal({
  		      title: "Erreur taille d'image",
  		      text: "¡La taille de l'image ne doit pas dépasser 2 Mo",
           icon: "warning",
  		      button: "Fermer!"
  		    });

  	}else{

  		var datosImage = new FileReader;
  		datosImage.readAsDataURL(image);

  		$(datosImage).on("load", function(event){

  			var routeImage = event.target.result;

  			$(".apercu").attr("src", routeImage);

  		})

  	}
})

/*=============================================
MODIFIER PRODUIT
=============================================*/

$(".tableProduits tbody").on("click", "button.btnModifierProduit", function(){

	var idProduit = $(this).attr("idProduit");

	var datos = new FormData();
    datos.append("idProduit", idProduit);

     $.ajax({

      url:"ajax/produits.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(reponse){

          var datosCategorie = new FormData();
          datosCategorie.append("idCategorie",reponse["id_categorie"]);

           $.ajax({

              url:"ajax/categories.ajax.php",
              method: "POST",
              data: datosCategorie,
              cache: false,
              contentType: false,
              processData: false,
              dataType:"json",
              success:function(reponse){

                  $("#modifCategorie").val(reponse["id"]);
                  $("#modifCategorie").html(reponse["categorie"]);

              }

          })

           $("#modifCode").val(reponse["code"]);

           $("#modifDescription").val(reponse["description"]);

           $("#modifStock").val(reponse["stock"]);

           $("#modifPrixAchat").val(reponse["prix_achat"]);

           $("#modifPrixVente").val(reponse["prix_vente"]);

           if(reponse["photo"] != ""){

           	$("#photoActuel").val(reponse["photo"]);

           	$(".apercu").attr("src",  reponse["photo"]);

           }

      }

  })

})

/*=============================================
SUPPRIMER PRODUIT
=============================================*/

$(".tableProduits tbody").on("click", "button.btnSupprimerProduit", function(){

	var idProduit = $(this).attr("idProduit");
	var code = $(this).attr("code");
	var photo = $(this).attr("photo");

  swal({
	  title: "Attention",
	  text: "Voulez-vous vraiment supprimer le produit?",
	  icon: "warning",
	  buttons: ["Annuler", "Confirmer"],

	}).then((result) => {
	  if (result) {
	    window.location = "index.php?lien=produits&idProduit="+idProduit+"&photo="+photo+"&code="+code;
	  }
	})
})
