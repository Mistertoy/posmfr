/*=============================================
Télécharger la Photo
=============================================*/
$(".nouvellePhoto").change(function(){

	var imagen = this.files[0];

	/*=============================================
  	VALIDATION DU FORMAT DE L'IMAGE: JPG OU PNG
  	=============================================*/

  	if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

  		$(".nouvellePhoto").val("");

  		 swal({
		      title: "Erreur de type d'image",
		      text: "¡L'image doit être au format JPG ou PNG!",
          icon: "warning",
          button: "Fermer!",

		    });

  	}else if(imagen["size"] > 2000000){

  		$(".nouvellePhoto").val("");

  		 swal({
		      title: "Erreur de taille d'image",
		      text: "¡La taille de l'image ne doit pas dépasser 2 Mo",
          icon: "warning",
		      button: "Fermer!"
		    });

  	}else{

  		var datosImagen = new FileReader;
  		datosImagen.readAsDataURL(imagen);

  		$(datosImagen).on("load", function(event){

  			var rutaImagen = event.target.result;

  			$(".apercu").attr("src", rutaImagen);

  		})

  	}
})

/*=============================================
MODIFIER UTILISATEUR
=============================================*/
$(document).on("click", ".btnModifierUser", function(){

	var idUser = $(this).attr("idUser");

	var datos = new FormData();
	datos.append("idUser", idUser);

	$.ajax({

		url:"ajax/utilisateurs.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(reponse){

			$("#modifierNom").val(reponse["nom"]);
			$("#modifierUser").val(reponse["user"]);
			$("#modifierProfil").html(reponse["profil"]);
			$("#modifierProfil").val(reponse["profil"]);
			$("#photoActuel").val(reponse["photo"]);

			$("#passwordActuel").val(reponse["password"]);

			if(reponse["photo"] != ""){

				$(".apercu").attr("src", reponse["photo"]);

			}

		}

	});

})

/*=============================================
ACTIVER L'UTILISATEUR
=============================================*/
$(document).on("click", ".btnActiver", function(){

	var idUser = $(this).attr("idUser");
	var etatUser = $(this).attr("etatUser");

	var datos = new FormData();
 	datos.append("activerId", idUser);
  	datos.append("activerUser", etatUser);

  	$.ajax({

	  url:"ajax/utilisateurs.ajax.php",
	  method: "POST",
	  data: datos,
	  cache: false,
      contentType: false,
      processData: false,
      success: function(reponse){

      	if(window.matchMedia("(max-width:767px)").matches){

      		 swal({
		      	title: "El usuario ha sido actualizado",
		      	type: "success",
		      	confirmButtonText: "¡Cerrar!"
		    	}).then(function(result) {

		        	if (result.value) {

		        	window.location = "utilisateurs";

		        }

		      });


		}
      }

  	})

  	if(etatUser == 0){

  		$(this).removeClass('btn-success');
  		$(this).addClass('btn-danger');
  		$(this).html('Désactivé');
  		$(this).attr('etatUser',1);

  	}else{

  		$(this).addClass('btn-success');
  		$(this).removeClass('btn-danger');
  		$(this).html('Activé');
  		$(this).attr('etatUser',0);

  	}

})

/*=============================================
VÉRIFIEZ SI L'UTILISATEUR EST DÉJÀ ENREGISTRÉ
=============================================*/

$("#nomUser").change(function(){

	$(".alert").remove();

	var utilisateur = $(this).val();

	var datos = new FormData();
	datos.append("validerUser", utilisateur);

	 $.ajax({
	    url:"ajax/utilisateurs.ajax.php",
	    method:"POST",
	    data: datos,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(reponse){

	    	if(reponse){

	    		$("#nomUser").parent().after('<div class="alert alert-warning">Cet utilisateur existe déjà dans la base de données</div>');

	    		$("#nomUser").val("");

	    	}

	    }

	})
})

/*=============================================
SUPPRIMER UTILISATEUR
=============================================*/
$(document).on("click", ".btnSupprimerUser", function(){

  var idUsuario = $(this).attr("idUser");
  var fotoUsuario = $(this).attr("photoUser");
  var usuario = $(this).attr("user");

	swal({
	  title: "Attention",
	  text: "Voulez-vous vraiment supprimer l'utilisateur?",
	  icon: "warning",
	  buttons: ["Annuler", "Confirmer"],

	}).then((result) => {
	  if (result) {
	    window.location = "index.php?lien=utilisateurs&idUser="+idUsuario+"&user="+usuario+"&photoUser="+fotoUsuario;
	  }
	})


})
