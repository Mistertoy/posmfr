<!-- SideBar Menu -->

<!-- Data table -->
$('.tables').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "language": {
                  	"sEmptyTable":     "Aucune donnée disponible dans le tableau",
                  	"sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                  	"sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                  	"sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                  	"sInfoPostFix":    "",
                  	"sInfoThousands":  ",",
                  	"sLengthMenu":     "Afficher _MENU_ éléments",
                  	"sLoadingRecords": "Chargement...",
                  	"sProcessing":     "Traitement...",
                  	"sSearch":         "Rechercher :",
                  	"sZeroRecords":    "Aucun élément correspondant trouvé",
                  	"oPaginate": {
                  		"sFirst":    "Premier",
                  		"sLast":     "Dernier",
                  		"sNext":     "Suivant",
                  		"sPrevious": "Précédent"
                  	},
                  	"oAria": {
                  		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                  		"sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                  	},
                  	"select": {
                          	"rows": {
                           		"_": "%d lignes sélectionnées",
                           		"0": "Aucune ligne sélectionnée",
                          		"1": "1 ligne sélectionnée"
                          	}
                  	}
                  }
    });


/*=============================================
 //input Mask
=============================================*/
//Datemask dd/mm/yyyy
$(":input").inputmask();
$('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'jj/mm/aaaa' })
//Datemask2 mm/dd/yyyy
$('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
//Money Euro
$('[data-mask]').inputmask()
