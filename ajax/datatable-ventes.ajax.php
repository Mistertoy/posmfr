<?php

require_once "../controllers/produits.controller.php";
require_once "../models/produits.model.php";


class tableProduitsVentes{

 	/*=============================================
 	 MOSTRAR LA TABLA DE PRODUCTOS
  	=============================================*/

	public function affichertableProduitsVentes(){

		$item = null;
    	$valeur = null;
    	$ordre = "id";

  		$produits = ControllerProduits::ctrAfficherProduits($item, $valeur, $ordre);

  		if(count($produits) == 0){

  			echo '{"data": []}';

		  	return;
  		}

  		$datosJson = '{
		  "data": [';

		  for($i = 0; $i < count($produits); $i++){

		  	/*=============================================
 	 		TRAEMOS LA IMAGEN
  			=============================================*/

		  	$photo = "<img src='".$produits[$i]["photo"]."' width='40px'>";

		  	/*=============================================
 	 		STOCK
  			=============================================*/

  			if($produits[$i]["stock"] <= 10){

  				$stock = "<button class='btn btn-danger'>".$produits[$i]["stock"]."</button>";

  			}else if($produits[$i]["stock"] > 11 && $produits[$i]["stock"] <= 15){

  				$stock = "<button class='btn btn-warning'>".$produits[$i]["stock"]."</button>";

  			}else{

  				$stock = "<button class='btn btn-success'>".$produits[$i]["stock"]."</button>";

  			}

		  	/*=============================================
 	 		ACTIONS
  			=============================================*/

		  	$botones =  "<div class='btn-group'><button class='btn btn-primary ajouterProduit recupererBouton' idProduit='".$produits[$i]["id"]."'>Ajouter</button></div>";

		  	$datosJson .='[
			      "'.($i+1).'",
			      "'.$photo.'",
			      "'.$produits[$i]["code"].'",
			      "'.$produits[$i]["description"].'",
			      "'.$stock.'",
			      "'.$botones.'"
			    ],';

		  }

		  $datosJson = substr($datosJson, 0, -1);

		 $datosJson .=   ']

		 }';

		echo $datosJson;


	}


}

/*=============================================
ACTIVER LE TABLEAU DES PRODUITS
=============================================*/
$activerProduitsVentes = new tableProduitsVentes();
$activerProduitsVentes -> afficherTableProduitsVentes();
