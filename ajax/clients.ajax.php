<?php

require_once "../controllers/clients.controller.php";
require_once "../models/clients.model.php";

class AjaxClients{

	/*=============================================
	EDITAR CLIENTE
	=============================================*/

	public $idClient;

	public function ajaxModifierClient(){

		$item = "id";
		$valeur = $this->idClient;

		$reponse = ControllerClients::ctrAfficherClients($item, $valeur);
		// echo "<script>
		// 				alert('".json_econde($reponse)."');
		// 			</script>";
		echo json_encode($reponse);




	}

}

/*=============================================
MODIFIER CLIENT
=============================================*/

if(isset($_POST["idClient"])){

	$client = new AjaxClients();
	$client -> idClient = $_POST["idClient"];
	$client -> ajaxModifierClient();

}
