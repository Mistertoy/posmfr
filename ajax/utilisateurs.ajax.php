<?php

require_once "../controllers/users.controller.php";
require_once "../models/users.model.php";

class AjaxUtilisateurs{

	/*=============================================
	FONCTION MODIFIER UTILISATEUR
	=============================================*/

	public $idUser;

	public function ajaxModifierUser(){

		$item = "id";
		$valeur = $this->idUser;

		$reponse =
		ControllerUsers::ctrAfficherUsers($item, $valeur);

		echo json_encode($reponse);

	}

	/*=============================================
	FONCTION ACTIVER UTILISATEUR
	=============================================*/

	public $activerUser;
	public $activerId;


	public function ajaxActiverUser(){

		$table = "users";

		$item1 = "etat";
		$valeur1 = $this->activerUser;

		$item2 = "id";
		$valeur2 = $this->activerId;

		$reponse = ModelUsers::mdlActualisationUser($table, $item1, $valeur1, $item2, $valeur2);

	}

	/*=============================================
	FONCTION VALIDATION RÉPÉTITION UTILISATEUR
	=============================================*/

	public $validerUser;

	public function ajaxValiderUser(){

		$item = "user";
		$valeur = $this->validerUser;

		$reponse = ControllerUsers::ctrAfficherUsers($item, $valeur);

		echo json_encode($reponse);

	}
}

/*=============================================
MODIFIER UTILISATEUR
=============================================*/
if(isset($_POST["idUser"])){

	$editar = new AjaxUtilisateurs();
	$editar -> idUser = $_POST["idUser"];
	$editar -> ajaxModifierUser();

}

/*=============================================
ACTIVER UTILISATEUR
=============================================*/

if(isset($_POST["activerUser"])){

	$activerUser = new AjaxUtilisateurs();
	$activerUser -> activerUser = $_POST["activerUser"];
	$activerUser -> activerId = $_POST["activerId"];
	$activerUser -> ajaxActiverUser();

}

/*=============================================
VALIDATION RÉPÉTITION UTILISATEUR
=============================================*/

if(isset( $_POST["validerUser"])){

	$valUser = new AjaxUtilisateurs();
	$valUser-> validerUser = $_POST["validerUser"];
	$valUser -> ajaxValiderUser();

}
