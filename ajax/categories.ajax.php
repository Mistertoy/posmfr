<?php

require_once "../controllers/categories.controller.php";
require_once "../models/categories.model.php";

class AjaxCategories{

	/*=============================================
	FONCTION MODIFIER CATEGORIE
	=============================================*/

	public $idCategorie;

	public function ajaxModifierCategorie(){

		$item = "id";
		$valeur = $this->idCategorie;

		$reponse = ControllerCategories::ctrAfficherCategories($item, $valeur);

		echo json_encode($reponse);

	}
}

/*=============================================
MODIFIER CATEGORIE
=============================================*/
if(isset($_POST["idCategorie"])){

	$categorie = new AjaxCategories();
	$categorie -> idCategorie = $_POST["idCategorie"];
	$categorie -> ajaxModifierCategorie();
}
