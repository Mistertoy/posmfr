<?php

require_once "../controllers/produits.controller.php";
require_once "../models/produits.model.php";

require_once "../controllers/categories.controller.php";
require_once "../models/categories.model.php";


class TableProduits{

 	/*=============================================
 	 MOSTRAR LA TABLA DE PRODUCTOS
  	=============================================*/

	public function afficherTableProduits(){

			$item = null;
    	$valeur = null;
			$ordre = "id";

  		$produits = ControllerProduits::ctrAfficherProduits($item, $valeur, $ordre);

			if(count($produits) == 0){

  			echo '{"data": []}';

		  	return;
  		}

  		$datosJson = '{
		  "data": [';

		  for($i = 0; $i < count($produits); $i++){

		  /*=============================================
 	 		PHOTO PRODUIT
  		=============================================*/

		  	$image = "<img src='".$produits[$i]["photo"]."' width='40px'>";

		  	/*=============================================
 	 			CATEGORIE
  			=============================================*/

		  	$item = "id";
		  	$valeur = $produits[$i]["id_categorie"];

		  	$categories = ControllerCategories::ctrAfficherCategories($item, $valeur);

		  	/*=============================================
 	 			STOCK
  			=============================================*/

  			if($produits[$i]["stock"] <= 10){

  				$stock = "<button class='btn btn-danger'>".$produits[$i]["stock"]."</button>";

  			}else if($produits[$i]["stock"] > 11 && $produits[$i]["stock"] <= 15){

  				$stock = "<button class='btn btn-warning'>".$produits[$i]["stock"]."</button>";

  			}else{

  				$stock = "<button class='btn btn-success'>".$produits[$i]["stock"]."</button>";

  			}

		  	/*=============================================
 	 		TRAEMOS LAS ACCIONES
  			=============================================*/

			if(isset($_GET["profilCache"]) && $_GET["profilCache"] == "Especial"){

		  	$boutons =  "<div class='btn-group'><button class='btn btn-warning btnModifierProduit' idProducto='".$produits[$i]["id"]."' data-toggle='modal' data-target='#modalModifierProduit'><i class='fas fa-pencil-alt'></i></button></div>";

			}else{
				$boutons =  "<div class='btn-group'><button class='btn btn-warning btnModifierProduit' idProduit='".$produits[$i]["id"]."' data-toggle='modal' data-target='#modalModifierProduit'><i class='fas fa-pencil-alt'></i></button><button class='btn btn-danger btnSupprimerProduit' idProduit='".$produits[$i]["id"]."' code='".$produits[$i]["code"]."' photo='".$produits[$i]["photo"]."'><i class='fas fa-trash-alt'></i></button></div>";
			}

		  	$datosJson .='[
			      "'.($i+1).'",
			      "'.$image.'",
			      "'.$produits[$i]["code"].'",
			      "'.$produits[$i]["description"].'",
			      "'.$categories["categorie"].'",
			      "'.$stock.'",
			      "'.$produits[$i]["prix_achat"].'",
			      "'.$produits[$i]["prix_vente"].'",
			      "'.$produits[$i]["date"].'",
			      "'.$boutons.'"
			    ],';

		  }

		  $datosJson = substr($datosJson, 0, -1);

		 $datosJson .=   ']

		 }';

		echo $datosJson;


	}


}

/*=============================================
ACTIVAR TABLA DE PRODUCTOS
=============================================*/
$activerProduits = new TableProduits();
$activerProduits -> afficherTableProduits();
