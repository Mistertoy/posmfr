<?php

require_once "../controllers/produits.controller.php";
require_once "../models/produits.model.php";

require_once "../controllers/categories.controller.php";
require_once "../models/categories.model.php";

class AjaxProduits{

  /*=============================================
  FONCTION GÉNÉRER CODE À PARTIR DE L'ID CATÉGORIE
  =============================================*/
  public $idCategorie;

  public function ajaxCreerCodeProduit(){

    $item = "id_categorie";
    $valeur = $this->idCategorie;

    $reponse = ControllerProduits::ctrAfficherProduits($item, $valeur, $ordre);

    echo json_encode($reponse);

  }


  /*=============================================
  FONCTION MODIFIER PRODUIT
  =============================================*/

  public $idProduit;
  public $apporterProduits;
  public $nomProduit;

  public function ajaxModifierProduit(){

    if($this->apporterProduits == "ok"){

    $item = "id";
    $valeur = $this->idProduit;

    $reponse = ControllerProduits::ctrAfficherProduits($item, $valeur, $ordre);

    echo json_encode($reponse);

  }else if($this->nomProduit != ""){
    $item = "description";
    $valeur = $this->nomProduit;
    $ordre = "id";

    $reponse = ControllerProduits::ctrAfficherProduits($item, $valeur, $ordre);

    echo json_encode($reponse);
  }else{
    $item = "id";
    $valeur = $this->idProduit;
    $ordre = "id";

    $reponse = ControllerProduits::ctrAfficherProduits($item, $valeur, $ordre);

    echo json_encode($reponse);
  }

  }

}


/*=============================================
GÉNÉRER CODE À PARTIR DE L'ID CATÉGORIE
=============================================*/

if(isset($_POST["idCategorie"])){

  $codeProduit = new AjaxProduits();
  $codeProduit -> idCategorie = $_POST["idCategorie"];
  $codeProduit -> ajaxCreerCodeProduit();

}
/*=============================================
MODIFIER PRODUIT
=============================================*/

if(isset($_POST["idProduit"])){

  $modifierProduit = new AjaxProduits();
  $modifierProduit -> idProduit = $_POST["idProduit"];
  $modifierProduit -> ajaxModifierProduit();

}

/*=============================================
APPORTER PRODUIT
=============================================*/

if(isset($_POST["apporterProduits"])){

  $apporterProduits = new AjaxProduits();
  $apporterProduits -> traerProductos = $_POST["apporterProduits"];
  $apporterProduits -> ajaxModifierProduit();

}

/*=============================================
APPORTER NOM PRODUIT
=============================================*/

if(isset($_POST["nomProduit"])){

  $apporterProduits = new AjaxProduits();
  $apporterProduits -> nomProduit = $_POST["nomProduit"];
  $apporterProduits -> ajaxModifierProduit();

}
